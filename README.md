Attorneys at Kemp, Ruge & Green Law Group handle many issues in many areas of personal injury law, including auto accidents, slip and falls, work related auto accidents, defective consumer products and more. Call or Text Us at 727-847-4878.

Address: 11567 Trinity Blvd, Trinity, FL 34655, USA

Phone: 727-847-4878

Website: [https://kemprugegreen.com](https://kemprugegreen.com)
